
public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Declarar variables
		int X=8;
		int Y=5;
		double N=7.3;
		double M=3.1;
		//imprimir variables
		System.out.println("Variable X = "+X+"\nVariable Y = "+Y+"\nVariable M = "+M+"\nVariable N = "+N );
		//suma 1
		System.out.println(X+"+"+Y+" = "+(X+Y));
		//resta 1
		System.out.println(X+"-"+Y+" = "+(X-Y));
		//multiplicacion
		System.out.println(X+"x"+Y+" = "+(X*Y));
		// division
		System.out.println(X+"/"+Y+" = "+(X/Y));
		// resto 1
		System.out.println(X+"/"+Y+" el resto igual a "+(X%Y));
		//suma 2
		System.out.println(N+"+"+M+" = "+(N+M));
		//resta 2
		System.out.println(N+"-"+M+" = "+(N-M));
		//multiplicacion 2
		System.out.println(N+"x"+M+" = "+(N*M));
		// division 2
		System.out.println(N+"/"+M+" = "+(N/M));
		// resto 2
		System.out.println(N+"/"+M+" el resto igual a "+(N%M));
		//dobles
		System.out.println("Dobles:");
		System.out.println("Variable X = "+X*2+"\nVariable Y = "+Y*2+"\nVariable M = "+M*2+"\nVariable N = "+N*2 );
		//suma totes
		System.out.println("La suma de todas la variables es = "+(X+Y+N+M));
		//multi totes
		System.out.println("La multiplicacion de todas la variables es = "+(X*Y*N*M));
		
		
	}

}
